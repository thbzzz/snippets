#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
  echo "run as root" 2>&1
  exit 1

cp theme/gnome-shell-theme.gresource /usr/share/gnome-shell/

echo "gdm will restart in 5 seconds"
sleep 5

systemctl restart gdm.service
